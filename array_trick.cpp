#include <iostream>
using namespace std;
#define MAX 5

void insert(int *arr, int n, int x, int i){
	if (i < n){
		int temp = x;
		for (int j = i; j < n; j++){
			int temp2 = arr[j];
			arr[j] = temp;
			temp = temp2;
		}
	} else 
		cout << "Invalid i" << endl;
}

int main(){
	int arr[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	insert(arr, 10, 12, 5);
	for (int i=0; i<10; i++) cout << arr[i] << " ";
	cout<<endl;
	return 0;
}