#include <iostream>
using namespace std;
#define MAX 5

int queue[MAX];
int front = -1, rear = -1;
int size = 0;

void insert(int x){
	if (size < MAX){
		if (rear == -1)
			front = rear = 0;
		else 
			rear = (rear + 1) % MAX;
		queue[rear] = x;
		size++;
	} else {
		cout<<"Queue full"<<endl;
	}
}

void del(){
	if (size > 0){
		size --;
		if (!size) front = rear = -1;
		else front = ( front + 1 ) % MAX;
	} else {
		cout<<"Nothing to delete"<<endl;
	}
}

void traverse(){
	for (int i=front, cnt = 0; i!= (rear+1)%MAX; i = (i+1)%MAX)
		cout<<queue[i]<<" ";
	cout<<endl;
}

int main(){
	insert(1);
	insert(2);
	insert(3);
	traverse();
	del();
	traverse();
}