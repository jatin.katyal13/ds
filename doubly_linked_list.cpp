#include <bits/stdc++.h>
using namespace std;

struct node {
	int id;
	string name;
	node *next, *prev;
};

class dl_list{
	private:
		node *top;
	public:
		void insert(int id, string name){
			node* temp = new node();
			temp->id = id;
			temp->name = name;
			if (top == NULL){
				temp->next = NULL;
				temp->prev = NULL;
				top = temp;
			} else {
				temp -> next = top;
				top -> prev = temp;
				top = temp;
			}
		}
		void del(int id){
			node *i;
			i = top;
			for (; i!=NULL; i = i->next){
				if (i->id == id){
					node *temp = i;
					if (temp == top){
						top -> next -> prev = NULL;
						top = top -> next;
					} else{
						i -> prev -> next = i -> next;
						i -> next -> prev = i -> prev;
					}
					delete temp;
					break;
				}
			}
		}
		void traverse(){
			for (node* i = top; i != NULL; i = i->next) cout << i->id << "-" << i->name << endl;
		}
};
int main(){
	dl_list l;
	l.insert(1, "jatin");
	l.insert(2, "tanya");
	l.insert(3, "anay");
	l.traverse();
	l.del(2);
	l.traverse();
	return 0;
}