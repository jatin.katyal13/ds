#include<bits/stdc++.h>
using namespace std;

struct node{
	int roll;
	string name;
	node *next;
};

class l_list {
	private:
		node *top;
	public:
		void insert(int roll, string name){
			node *temp = new node();
			temp -> roll = roll;
			temp -> name = name;
			if (top == NULL)
				top = temp;
			else {
				temp->next = top;
				top = temp;
			}
		}
		void del(int roll){
			node *i, *prev;
			i = top;
			prev = NULL;
			for(; i != NULL; prev = i, i = i->next){
				if (i->roll == roll){
					node* temp = i;
					if (prev){
						prev -> next = i -> next;
					} else {
						top = i -> next;
					}
					delete temp;
				}
			}
		}
		void traverse(){
			for (node* i = top; i != NULL; i = i->next) cout << i->roll << "-" << i->name << endl;
		}
};

int main(){
	l_list l;
	l.insert(1, "jatin");
	l.insert(2, "tanya");
	l.insert(3, "anay");
	l.traverse();
	l.del(2);
	l.traverse();
	return 0;
}