#include <iostream>
using namespace std;

void ssort(int *arr, int s)
{
	int i,j,temp,small;
	for(i=0;i<s-1;i++)
	{
		small=i;
		for(j=i+1;j<s;j++) 	
		if(arr[j]<arr[small])
			small=j;
		if(small!=i)
		{
			temp=arr[i]; 		
			arr[i]=arr[small];
			arr[small]=temp;
		}
	}
}

void bsort(int *arr,int s)
{
	int i,j,temp;
	for(i=0;i<s-1;i++)
	{
		for(j=0;j<(s-1-i);j++)
			if(arr[j]>arr[j+1])
			{
				temp=arr[j]; 	
				arr[j]=arr[j+1];
				arr[j+1]=temp;
			}
	}
}

int main(){
	int a[] = {5, 2, 3, 5, 1, 4};
	ssort(a, 6);
	for (int i=0; i<6; i++) cout<<a[i]<<" ";
	cout<<endl;
	int b[] = {4, 1, 6 ,2, 6, 3};
	bsort(b, 6);
	for (int i=0; i<6; i++) cout<<b[i]<<" ";
	cout<<endl;
}