#include <iostream>
using namespace std;
#define MAX 5

int stack[MAX];
int top = -1;

void insert(int x){
	if (top < MAX - 1){
		stack[++top] = x;
	} else {
		cout<<"Stack Overflow"<<endl;
	}
}

void pop(){
	if (top >= 0)
		top--;
}

void traverse(){
	for (int i = top; i>=0; i--) cout<<stack[i]<<" ";
	cout<<endl;
}

int main(){
	insert(1);
	insert(2);
	insert(3);
	traverse();
	pop();
	traverse();
}